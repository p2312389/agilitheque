module org.example.agilitheque {
    requires javafx.controls;
    requires javafx.fxml;

    requires org.controlsfx.controls;
    requires net.synedra.validatorfx;
    requires java.sql;
    requires mysql.connector.j;

    opens org.example.agilitheque to javafx.fxml;
    exports org.example.agilitheque;
}