package org.example.agilitheque.modele;

public class Genre {
    private int idGenre;
    private String libelle;

    // Constructeur sans arguments
    public Genre() {}

    // Constructeur avec arguments
    public Genre(int idGenre, String libelle) {
        this.idGenre = idGenre;
        this.libelle = libelle;
    }

    // Getters et Setters
    public int getIdGenre() {
        return idGenre;
    }

    public void setIdGenre(int idGenre) {
        this.idGenre = idGenre;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    // Méthode toString
    @Override
    public String toString() {
        return "Genre [idGenre=" + idGenre + ", libelle=" + libelle + "]";
    }
}

