package org.example.agilitheque.modele;

public class Auteur {
    private int idAuteur;
    private String prenom;
    private String nom;

    // Constructeur sans arguments
    public Auteur() {}

    // Constructeur avec arguments
    public Auteur(int idAuteur, String prenom, String nom) {
        this.idAuteur = idAuteur;
        this.prenom = prenom;
        this.nom = nom;
    }

    // Getters et Setters
    public int getIdAuteur() {
        return idAuteur;
    }

    public void setIdAuteur(int idAuteur) {
        this.idAuteur = idAuteur;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    // Méthode toString
    @Override
    public String toString() {
        return "Auteur [idAuteur=" + idAuteur + ", prenom=" + prenom + ", nom=" + nom + "]";
    }
}
