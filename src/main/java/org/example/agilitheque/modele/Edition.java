package org.example.agilitheque.modele;

public class Edition {
    private int idEdition;
    private String nomEdition;

    // Constructeur sans arguments
    public Edition() {}

    // Constructeur avec arguments
    public Edition(int idEdition, String nomEdition) {
        this.idEdition = idEdition;
        this.nomEdition = nomEdition;
    }

    // Getters et Setters
    public int getIdEdition() {
        return idEdition;
    }

    public void setIdEdition(int idEdition) {
        this.idEdition = idEdition;
    }

    public String getNomEdition() {
        return nomEdition;
    }

    public void setNomEdition(String nomEdition) {
        this.nomEdition = nomEdition;
    }

    // Méthode toString
    @Override
    public String toString() {
        return "Edition [idEdition=" + idEdition + ", nomEdition=" + nomEdition + "]";
    }
}
