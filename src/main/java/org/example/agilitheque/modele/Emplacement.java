package org.example.agilitheque.modele;

public class Emplacement {
    private String emp;

    // Constructeur sans arguments
    public Emplacement() {}

    // Constructeur avec arguments
    public Emplacement(String emp) {
        this.emp = emp;
    }

    // Getters et Setters
    public String getEmp() {
        return emp;
    }

    public void setEmp(String emp) {
        this.emp = emp;
    }

    // Méthode toString
    @Override
    public String toString() {
        return "Emplacement [emp=" + emp + "]";
    }
}
