package org.example.agilitheque.utils;

import java.sql.DriverManager;
import java.sql.*;

public class DatabaseConnection {
    private static Connection connection = null;
    static {
        String nomDB = "agilitheque";
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");

            //étape 2: créer l'objet de connexion
            connection = DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/"+nomDB, "root", "");
        }catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
    }
    public static Connection getConnection()
    {
        return connection;
    }

}
