package org.example.agilitheque;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.scene.image.Image;
import org.example.agilitheque.controleur.TestControleur;

import java.io.IOException;

public class HelloApplication extends Application {
    @Override
    public void start(Stage stage) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(HelloApplication.class.getResource("vue/hello-view.fxml"));
        Scene scene = new Scene(fxmlLoader.load(), 320, 240);
        stage.setTitle("Agilithèque!");
        Image icon = new Image(HelloApplication.class.getResourceAsStream("/org/example/agilitheque/vue/agilitheque.jpg"));
        stage.getIcons().add(icon);
        stage.setScene(scene);
        stage.show();
        TestControleur.fonctionTest();
    }

    public static void main(String[] args) {
        launch();
    }
}